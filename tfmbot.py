#-*- encoding: utf8 -*-
#!/usr/bin/python
##############################################
##        Transformice IRC Robot            ##
##############################################
##           Created by Aha2Y               ##
##############################################

from twisted.words.protocols import irc
from twisted.internet import reactor, protocol
from time import sleep, time, gmtime, strftime
from twisted.python import log
import datetime
import logging
import time
import sys
import pkgutil
import imp
import json
import urllib2
import socket
import ConfigParser, json
from modules import mice_stats

config = ConfigParser.ConfigParser()
config.read("configuration.ini")

class IRCbot(irc.IRCClient):
    nickname = config.get('client', 'nick', 0)
    realname = config.get('client', 'gecos', 0)
    username = config.get('client', 'user', 0)
    nickpass = config.get('client', 'nspass', 0)
    sourceURL = config.get('ctcp', 'source', 0)
    versionName = config.get('ctcp', 'version', 0)
    versionNum = config.get('client', 'version', 1)

    def formatLog(self, message, level):
        timestamp = '[' +'\x1b[37m' + datetime.datetime.now().strftime("%a %d,%H:%M:%S") + '\x1b[0m'
        if level == 0:
            return "%s|\x1b[33mVerbose\x1b[0m] %s" % (timestamp, message)
        if level == 1:
            return "%s|\x1b[36mUnknown command\x1b[0m] %s" % (timestamp, message)

    def connectionMade(self):
        irc.IRCClient.connectionMade(self)

    def connectionLost(self, reason):
        irc.IRCClient.connectionLost(self, reason)

    def irc_PING(self, prefix, params):
        self.sendLine("PONG %s" % params[-1])

    def signedOn(self):
        print self.formatLog("Signed on to IRC server.", 0)
        self.join(self.factory.channels)
        print self.formatLog("Joined channel", 0)

    def irc_NOTICE(self, prefix, params):
        if "This nickname is registered and protected. If it is your" in params:
            self.formatLog("Nickserv password request received", 0)
            self.msg("NickServ", "identify %s" % self.nickpass)
            print self.formatLog("Successfuly identified.", 0)

    def privmsg(self, user, channel, msg):
        prefix = config.get('client', 'prefix', 0)
        user = user.split('!', 1)[0]
        if msg.startswith(prefix):
            x = msg.split()
            cmd = x[0].strip(prefix)

            if cmd.startswith("ping"):
                self.msg(channel, "ping")

            if cmd.startswith("p"):
                debug = msg.split()
                try:
                    x = debug[1]
                except IndexError: 
                    x = user
                try:
                    player = mice_stats.player_by_name(x) 
                    title = player.titlelist[player.micetitle]
                    line = "{1} «{2}» ({3}) rounds played: {4} :: cheese collected: {5} :: first gained: {6} (mice saved: {6} :: cheese collected while shaman: {7})"
                    if not title:
                        title = "???"
                    self.msg(channel, line.format("", player.name, title, player.tribe, player.rounds, player.cheese, player.first, player.chamansave, player.chamancheese))
                except mice_stats.TransformiceError:
                    self.msg(channel, "Error: Unknown player.")

            if cmd.startswith("r"):
                debug = msg.split()
                try:
                    x = debug[1]
                except IndexError: 
                    x = user
                try:
                    player = x
                    data = json.load(urllib2.urlopen('http://api.formice.com/mouse/all-ranks.json?n=' + player))
                    line = "{0} :: rank: {1} :: rounds: {2} :: cheese {3} :: shaman: {4} :: saves: {5} :: hard saves: {6} :: first: {7}" 
                    self.msg(channel, line.format(x, data['rank'].encode("utf-8"), data['rounds_rank'].encode("utf-8"), data['cheese_rank'].encode("utf-8"), data['sham_cheese_rank'].encode("utf-8"), data['saves_rank'].encode("utf-8"), data['hard_saves_rank'].encode("utf-8"), data['first_rank'].encode("utf-8")))
                except KeyError:
                    self.msg(channel, "Unknown mice.")

            else:
                print self.formatLog("<%s> %s" % (user ,cmd), 1)

        if msg.startswith(self.nickname + ":"):
            x = msg.split(':')[1]
            cmd = x[1:]
            params = cmd.split()
            print params
            print len(params)

    def alterCollidedNick(self, nickname):
        return nickname + '^'

class IRCbotFactory(protocol.ReconnectingClientFactory): 

    protocol = IRCbot()
    channels = config.get('client', 'channels', 0)

    def __init__(self, channel):
        self.channel = channel

    def buildProtocol(self, addr):
        p = IRCbot()
        p.factory = self
        return p

    def clientConnectionLost(self, connector, reason):
        """If we get disconnected, reconnect to server."""
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print "connection failed:", reason
        reactor.stop()

if __name__ == '__main__':
    f = IRCbotFactory("socket")
    reactor.connectTCP(config.get('connect', 'hostname', 0), config.getint('connect', 'port'), f)
    reactor.run()
